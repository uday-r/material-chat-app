var express = require('express');
var app = express();
var server = app.listen(process.env.PORT || 8089);
var io = require('socket.io').listen(server);

app.use(express.static(__dirname + '/client/'));

var conferences = []
io.sockets.on('connection', function (client) {


	console.log("Connection established")

	//Sending live conferences to guest.
	client.emit("liveConferences", conferences);

	var _checkIfConferenceAvailable = function(conferenceName) {
		console.log("Checking if conference available.", conferenceName);
		var foundConferenceID = conferences.indexOf(conferenceName);
		if(foundConferenceID != -1) {
			console.log("Conference already found ", conferenceName);
		}
		return foundConferenceID;
	}


    client.on('createConference', function (conferenceName) {
		var found = _checkIfConferenceAvailable(conferenceName);
		if(found != -1) {
			console.log("Tried to create duplicate conference: ", conferenceName);
			client.emit("ERROR", {'code': 1, 'msg': "Conference already created please try another name."});
			return;
		}
		conferences.push(conferenceName);
		console.log('Added new conference to conferences ', conferences)
		client.emit("conferenceCreated", {'createdRoom': conferenceName, 'conferences': conferences});
		client.broadcast.emit("newConferenceCreated", conferenceName);
    });

    client.on('joinConference', function (conferenceName) {
		var found = _checkIfConferenceAvailable(conferenceName);
		if(found == -1) {
			console.log("Conference not found: ", conferenceName);
			client.emit("ERROR", {'code': 2, 'msg': "Conference not found."});
			return;
		}
		console.log("Can join room ", conferenceName);
		client.emit("conferenceJoined", {'createdRoom': conferenceName, 'conferences': conferences});
    });

	client.on('endConference', function(conferenceName) {
		console.log("Deleting conference", conferenceName)
		var foundConferenceID = _checkIfConferenceAvailable(conferenceName);
		if(foundConferenceID != -1) {
			console.log("removing conference");
			conferences.splice(foundConferenceID, 1);
			client.broadcast.emit("conferenceEnded", conferenceName);
		}
	});
});

