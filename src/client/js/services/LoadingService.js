var conferenceApp = angular.module('Conference');
conferenceApp.factory('Loading',['$mdDialog', '$mdMedia', function ($mdDialog, $mdMedia) {

    return {
		start: function() {
			$mdDialog.show({
				templateUrl: 'js/templates/loading.html',
				hasBackdrop: true
			});
		},
		stop: function() {
			$mdDialog.hide();
		}
    };
}]);
