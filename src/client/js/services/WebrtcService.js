var videoChat = angular.module('Conference');
videoChat.factory('Webrtc',['Loading', '$timeout', function (Loading, $timeout) {
	var webrtc = null;
	return {
		createConference: function(roomName) {
			document.getElementById("video-containers").innerHTML = "<div><span class=\"video-type\">Your Conference is live</span></div><video id=\"local-video\"></video>";
			webrtc = new SimpleWebRTC({
				localVideoEl: 'local-video',
				autoRequestMedia: true,
				localVideo: {
				   mirror: true
				}
			});
			webrtc.joinRoom(roomName);
			//Loading.start()

			//$timeout(function() {
				//Loading.stop()
			//}, 1);
		},

		joinConference: function(roomName){
			document.getElementById("video-containers").innerHTML = "<div id=\"remote-video\"><div><span class=\"video-type\">Live stream</span></div></div>";
			webrtc = new SimpleWebRTC({
				autoRequestMedia: true,
				remoteVideosEl: 'remote-video'
			});
			webrtc.startRemoteFeeds();
			webrtc.joinRoom(roomName);
			Loading.start()

			webrtc.on('videoAdded', function(sessionId) {
				Loading.stop()
				webrtc.stopRemoteFeeds();
			});
		},

		leaveConference: function(){
			document.getElementById("video-containers").innerHTML = "";
			if(webrtc) {
				webrtc.leaveRoom();
			}
		},

		startRemoteFeeds: function() {
			webrtc.startRemoteFeeds();
		},
	}
}]);
