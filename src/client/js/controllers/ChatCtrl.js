angular.module('Conference')
.controller("ChatCtrl", [
			'$scope'
			, '$location'
			, '$window'
			, '$timeout'
			, '$q'
			, 'Socket'
			, 'Webrtc'
			, '$mdSidenav'
			, '$mdMedia'
			, '$mdDialog'
			, function(
				$scope
				, $location
				, $window
				, $timeout
				, $q
				, Socket
				, Webrtc
				, $mdSidenav
				, $mdMedia
				, $mdDialog){

					
					$scope.$mdMedia = $mdMedia;
					var initializing = true
					Socket.connect($location.absUrl());
					$scope.activeConferences = [];
					$scope.admin = false;

					$scope.toggleMirror = function() {
						var video = document.getElementsByTagName('video')[0]
						video.className += "mirrior-feed";
					}

					window.onbeforeunload = function(e) {
						leaveConference();
						return "Conference " + $scope.currentConference + " is deleted, please create new one."
					}

					var leaveConference = function() {
						Webrtc.leaveConference($scope.currentConference);
						if($scope.admin) {
							$scope.admin = false;
							Socket.emit('endConference', $scope.currentConference);
						}
					}

					var conferenceCreateOrJoin = function() {
						$mdDialog.show({
							templateUrl: 'js/templates/addOrJoinRoom.html',
							controller: 'ConferenceActionController',
							fullscreen: $mdMedia('xs') || $mdMedia('sm'),
							escapeToClose: false,
							scope: $scope,
							preserveScope: true,
							//locals: {
								//'activeConferences': $scope.activeConferences
							//}
						});
					}

					Socket.on("conferenceCreated", function(conferencesMap) {
						//Initiate conference video
						$scope.admin = true;
						$scope.currentConference = conferencesMap.createdRoom;
						Webrtc.createConference(conferencesMap.createdRoom);
						$scope.activeConferences = conferencesMap.conferences;
					});

					Socket.on("conferenceJoined", function(conferencesMap) {
						//Stream conference video
						$scope.currentConference = conferencesMap.createdRoom;
						Webrtc.joinConference(conferencesMap.createdRoom);
						$scope.activeConferences = conferencesMap.conferences;
					});

					Socket.on("newConferenceCreated", function(conferenceName) {
						//Add it to active conferences
						$scope.activeConferences.push(conferenceName);
					});

					Socket.on("conferenceEnded", function(conferenceName) {
						console.log("Conference ended", conferenceName);
						var removedConfId = $scope.activeConferences.indexOf(conferenceName);
						$scope.activeConferences.splice(removedConfId, 1);
					});

					Socket.on("liveConferences", function(activeConferences) {
						$scope.activeConferences = activeConferences;
					});

					Socket.on("ERROR", function(errorDetails) {
						var alert = $mdDialog.alert()
							.title('Error')
							.textContent(errorDetails.msg)
							.ok('Close');
						$mdDialog
							.show( alert )
							.then(function() {
							}, function() {
							}).finally(function() {
								conferenceCreateOrJoin();
							});
					});

					$scope.joinConference = function(conferenceName) {
						if($mdMedia('xs') || $mdMedia('sm')) {
							$scope.hideSideBar();
						}
						if($scope.currentConference == conferenceName) {
							return;
						}
						leaveConference();
						Socket.emit('joinConference', conferenceName);
					}

					$scope.changeConference = function() {
						leaveConference()
						conferenceCreateOrJoin();
					}

					$scope.showSideBar = function() {
						$mdSidenav('left').open();
					}
					
					$scope.hideSideBar = function() {
						$mdSidenav('left').close();
					}

					conferenceCreateOrJoin();
}]);

angular.module('Conference')
.controller("ConferenceActionController", [
		'$scope'
		, '$mdDialog'
		, 'Socket'
		, function(
			$scope
			, $mdDialog
			, Socket) {
				$scope.conference = { create: true};
				$scope.closeDialog = function() {
					if($scope.conference.create == 1) {
						Socket.emit('createConference', $scope.conference.name);
					} else if($scope.conference.create == 0) {
						Socket.emit('joinConference', $scope.conference.name);
					}
					$mdDialog.hide();
				}
}]);
