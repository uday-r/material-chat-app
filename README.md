```html
How to install:
1. Download source code.
2. Hope you have node and npm, then in root directory run "npm install", that installs all dependencies.
3. Then from the same place run "node src/server.js", thats it your server is up.

Open up your browser and hit http://localhost:8089/


Description:
This is a simple video chat application demonstration for webrtc.
Technologies used:
NodeJs
AngularJs
Material-Angular
WebRtc
```
